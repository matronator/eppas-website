$(document).ready(function() {
  var smooth = new SmoothScroll('a[href*="#"]:not(.btn-gallery)', {
    speed: 500,
    speedAsDuration: false,
    durationMax: 750,
    durationMin: 400
  });

  $("#gallery-main").click(function() {
    openGallery();
  });
  $("#gallery-acc").click(function() {
    openGalleryAcc();
  });

  var slider = tns({
    container: ".review-slider",
    controlsContainer: ".slider-controls",
    items: 1,
    nav: false,
    center: true,
    startIndex: 1,
    responsive: {
      768: {
        items: 2
      },
      992: {
        items: 3
      }
    }
  });

  if($('.headroom')[0]) {
    var headroom  = new Headroom(document.querySelector("#navbar-main"), {
        offset: 300,
        tolerance : {
            up : 30,
            down : 30
        },
    });
    headroom.init();
  }
});

// Default galerie
function openGallery() {
  var pswpElement = document.querySelectorAll('.pswp')[0];
  // build items array
  var items = [
    {
      src: '/assets/img/main_gallery/main-01.jpg',
      w: 4592,
      h: 3056
    },
    {
      src: '/assets/img/main_gallery/main-02.jpg',
      w: 4592,
      h: 3056
    },
    {
      src: '/assets/img/main_gallery/main-03.jpg',
      w: 4592,
      h: 3056
    },
    {
      src: '/assets/img/main_gallery/main-04.jpg',
      w: 10872,
      h: 3315
    }
  ];
  // define options (if needed)
  var options = {
    // optionName: 'option value'
    // for example:
    index: 0 // start at first slide
  };
  // Initializes and opens PhotoSwipe
  var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
  gallery.init();
}

// Accomodation galerie
function openGalleryAcc() {
  var pswpElement = document.querySelectorAll('.pswp')[0];
  // build items array
  var items = [
    {
      src: '/assets/img/accommodation/acc-01.jpg',
      w: 640,
      h: 426
    },
    {
      src: '/assets/img/accommodation/acc-02.jpg',
      w: 3872,
      h: 2592
    },
    {
      src: '/assets/img/accommodation/acc-03.jpg',
      w: 2074,
      h: 1382
    },
    {
      src: '/assets/img/accommodation/acc-04.jpg',
      w: 2074,
      h: 1382
    }
  ];
  // define options (if needed)
  var options = {
    // optionName: 'option value'
    // for example:
    index: 0 // start at first slide
  };
  // Initializes and opens PhotoSwipe
  var gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
  gallery.init();
}
